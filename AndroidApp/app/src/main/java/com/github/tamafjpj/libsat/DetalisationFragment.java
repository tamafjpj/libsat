package com.github.tamafjpj.libsat;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.UUID;

public class DetalisationFragment extends Fragment {
    private static final String ARG_ITEM_ID = "item_id";
    private static final String ARG_ITEM_TYPE = "item_type";
    private TextView mTextView;
    private TextView mVarTextView;
    private DataSource ds = DataSource.INSTANCE;

    public static DetalisationFragment newInstance(UUID itemId, String itemType) {
        Bundle args = new Bundle();
        args.putSerializable(ARG_ITEM_ID, itemId);
        args.putString(ARG_ITEM_TYPE, itemType);
        DetalisationFragment fragment = new DetalisationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        UUID id = null;
        String type = null;
        Bundle bundle = this.getArguments();
        ImageButton imBtn;
        if (bundle != null) {
            id = (UUID) bundle.getSerializable(ARG_ITEM_ID);
            type = bundle.getString(ARG_ITEM_TYPE);
        }
        View view = null;
        switch (type) {
            case "Lecture": {
                view = inflater.inflate(R.layout.fragment_lecture_detalisation, container, false);
                mTextView = (TextView) view.findViewById(R.id.lecture_textView);
                mTextView.setText(ds.getItem(id).getDescription());
                imBtn=view.findViewById(R.id.imageButton);
                imBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(DetalisationFragment.this.getActivity(), WebViewActivity.class);
                        startActivity(intent);
                    }
                });
                break;
            }
            case "Practice": {
                view = inflater.inflate(R.layout.fragment_practice_detalisation, container, false);
                mTextView = (TextView) view.findViewById(R.id.practice_textView);
                mTextView.setText(ds.getItem(id).getDescription());
                mVarTextView = (TextView) view.findViewById(R.id.var_textView);
                mVarTextView.setText(R.string.var);
                mVarTextView.append(" 1337");
                break;
            }
            case "Test": {
                view = inflater.inflate(R.layout.fragment_test_detalisation, container, false);
                mTextView = (TextView) view.findViewById(R.id.test_textView);
                mTextView.setText(ds.getItem(id).getDescription());
                break;
            }
            default: {
                break;
            }
        }

        return view;
    }
}
