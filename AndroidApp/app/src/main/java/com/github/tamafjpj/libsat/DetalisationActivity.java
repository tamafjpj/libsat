package com.github.tamafjpj.libsat;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;


import java.util.UUID;


public class DetalisationActivity extends AppCompatActivity {
    private static final String EXTRA_ITEM_ID = "item_id";
    private static final String EXTRA_ITEM_TYPE = "item_type";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String typeOfDetalisation = getIntent().getStringExtra(EXTRA_ITEM_TYPE);
        setContentView(R.layout.activity_detalisation);
        Toolbar myChildToolbar =
                (Toolbar) findViewById(R.id.detalisation_toolbar);
        setSupportActionBar(myChildToolbar);

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);


        FragmentManager fm = getSupportFragmentManager();
        Fragment fragment = fm.findFragmentById(R.id.fragmentContainer);
        if (fragment == null) {

            fragment = DetalisationFragment.newInstance((UUID) getIntent().getSerializableExtra(EXTRA_ITEM_ID), typeOfDetalisation);
            fm.beginTransaction()
                    .add(R.id.fragmentContainer, fragment)
                    .commit();
        }

    }

}
