package com.github.tamafjpj.libsat;

public class Lecture extends Item {
    public Lecture(String name, String description, String source) {
        super("Lecture", name, description);
        super.setSource(source);
    }

}