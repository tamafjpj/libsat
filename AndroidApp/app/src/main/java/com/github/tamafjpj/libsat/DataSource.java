package com.github.tamafjpj.libsat;

import android.os.AsyncTask;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static android.content.ContentValues.TAG;

public enum DataSource {
    INSTANCE;
    private List<Lecture> lectures = new ArrayList<>(0);
    private List<Practice> practices = new ArrayList<>(0);
    private List<Test> tests = new ArrayList<>(0);

    DataSource() {
    }


    public List<Lecture> getLectures() {
        return lectures;
    }

    public List<Practice> getPractices() {
        return practices;
    }

    public List<Test> getTests() {
        return tests;
    }

    public List<Item> getItemList() {
        List<Item> buf = new ArrayList<>(0);
        buf.addAll(lectures);
        buf.addAll(practices);
        buf.addAll(tests);
        return buf;
    }

    public Item getItem(UUID id) {
        List<Item> itemList = getItemList();
        for (Item i : itemList) {
            if (i.getId().equals(id)) return i;
        }
        return null;
    }

    public void setLectures(List<Lecture> lectures) {
        if (this.lectures.isEmpty()) {
            for (Lecture i : lectures) {
                this.lectures.add(new Lecture(i.getName(), i.getDescription(), i.getSource()));
            }
        }
    }

    public void setPractices(List<Practice> practises) {
        if (this.practices.isEmpty()) {
            for (Practice i : practises) {
                this.practices.add(new Practice(i.getName(), i.getDescription(), i.getSource()));
            }
        }
    }

    public void setTests(List<Test> tests) {
        if (this.tests.isEmpty()) {
            for (Test i : tests) {
                this.tests.add(new Test(i.getName(), i.getDescription(), i.getSource()));
            }
        }
    }

    public void clear() {
        practices.removeAll(practices);
        lectures.removeAll(lectures);
        tests.removeAll(tests);
    }

}
