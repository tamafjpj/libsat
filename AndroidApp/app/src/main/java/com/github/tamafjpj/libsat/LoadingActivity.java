package com.github.tamafjpj.libsat;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;
import static java.lang.Thread.sleep;


public class LoadingActivity extends AppCompatActivity {
    ImageView mImageView;
    DataSource ds = DataSource.INSTANCE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loading);
        mImageView = (ImageView) findViewById(R.id.loading_image);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        class FetchItemsJSON extends AsyncTask<Void, Void, List<String>> {
            DataSource ds = DataSource.INSTANCE;
            private final String SERVER_ADRESS = "192.168.0.103";
            private final String SERVER_PORT = "8080";

            @Override
            protected List<String> doInBackground(Void... voids) {
                try {
                    sleep(1000);
                } catch (InterruptedException e) {/**/}
                List<String> resultSet = new ArrayList<>(0);
                try {
                    String result = getUrlString("http://" + SERVER_ADRESS + ":" + SERVER_PORT + "/?lectures=РЭШ&");
                    resultSet.add(result);
                    Log.i(TAG, "Fetched contents of URL: " + result);
                    result = getUrlString("http://" + SERVER_ADRESS + ":" + SERVER_PORT + "/?practices=РЭШ&");
                    resultSet.add(result);
                    Log.i(TAG, "Fetched contents of URL: " + result);
                /*result=getUrlString("http://"+SERVER_ADRESS+":"+SERVER_PORT+"/?tests=РЭШ&");
                ds.setTests(parseTests(result));*/
                } catch (IOException ioe) {
                    Log.e(TAG, "Failed to fetch URL: ", ioe);
                }
                return resultSet;
            }

            @Override
            protected void onPostExecute(List<String> list) {
                if (!list.isEmpty()) {
                    ds.setLectures(parseLectures(list.get(0)));
                    ds.setPractices(parsePractices(list.get(1)));
                    //ds.setTests(parseTests(list.get(2)));
                    startActivity(new Intent(LoadingActivity.this, MainActivity.class));
                } else {
                    Toast noConToast = Toast.makeText(getApplicationContext(), R.string.noConToast, Toast.LENGTH_LONG);
                    noConToast.show();
                    try {
                        sleep(200);
                    } catch (InterruptedException e) {/**/}
                    finish();
                }
            }

            public byte[] getUrlBytes(String urlSpec) throws IOException {
                URL url = new URL(urlSpec);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                try {
                    ByteArrayOutputStream out = new ByteArrayOutputStream();
                    InputStream in = connection.getInputStream();
                    if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                        throw new IOException(connection.getResponseMessage() +
                                ": with " +
                                urlSpec);
                    }
                    int bytesRead = 0;
                    byte[] buffer = new byte[1024];
                    while ((bytesRead = in.read(buffer)) > 0) {
                        out.write(buffer, 0, bytesRead);
                    }
                    out.close();
                    return out.toByteArray();
                } finally {
                    connection.disconnect();
                }

            }

            public String getUrlString(String urlSpec) throws IOException {
                return new String(getUrlBytes(urlSpec));
            }

            private List<Lecture> parseLectures(String json) {
                Gson gson = new Gson();
                List<Lecture> lectureList;
                Type type = new TypeToken<List<Lecture>>() {
                }.getType();
                lectureList = gson.fromJson(json, type);
                return lectureList;
            }

            private List<Practice> parsePractices(String json) {
                Gson gson = new Gson();
                List<Practice> practiceList;
                Type type = new TypeToken<List<Practice>>() {
                }.getType();
                practiceList = gson.fromJson(json, type);
                return practiceList;
            }

            private List<Test> parseTests(String json) {
                Gson gson = new Gson();
                List<Test> testList;
                Type type = new TypeToken<List<Test>>() {
                }.getType();
                testList = gson.fromJson(json, type);
                return testList;
            }

        }

        new FetchItemsJSON().execute();

    }
}
