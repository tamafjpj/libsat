package com.github.tamafjpj.libsat;

public class Practice extends Item {
    private String status = "Не выполнено";

    public Practice(String name, String description, String source) {
        super("Practice", name, description);
        super.setStatus(status);
        super.setSource(source);
    }
}
