package com.github.tamafjpj.libsat;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class PlaceholderFragment extends Fragment {

    private static final String ARG_SECTION_NUMBER = "section_number";
    private static final String EXTRA_ITEM_ID = "item_id";
    private static final String EXTRA_ITEM_TYPE = "item_type";
    private ItemAdapter mAdapter;
    private RecyclerView recyclerView;

    public static PlaceholderFragment newInstance(int sectionNumber) {
        PlaceholderFragment fragment = new PlaceholderFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public PlaceholderFragment() {
    }

    public Intent newIntent(Context context, UUID id, String type) {
        Intent intent = new Intent(context, DetalisationActivity.class);
        intent.putExtra(EXTRA_ITEM_ID, id);
        intent.putExtra(EXTRA_ITEM_TYPE, type);
        return intent;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.items_recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        updateUI(getArguments().getInt(ARG_SECTION_NUMBER));
        return recyclerView;
    }

    private class ItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private TextView mName;
        private TextView mStatus;
        private Item mItem;

        public ItemHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            mName = (TextView) itemView.findViewById(R.id.list_item_main_name);
            mStatus = (TextView) itemView.findViewById(R.id.list_item_main_status);
        }

        public void bindItem(Item item) {
            mItem = item;
            mName.setText(item.getName());
            mStatus.setText(item.getStatus());
            System.out.println(item.getId());
        }

        @Override
        public void onClick(View v) {
            Intent intent = newIntent(getActivity(), mItem.getId(), mItem.getType());
            startActivity(intent);
        }

    }

    private class ItemAdapter extends RecyclerView.Adapter<PlaceholderFragment.ItemHolder> {
        private List<Item> mItems;

        public ItemAdapter(List<Item> items) {
            mItems = items;
        }

        @Override
        public PlaceholderFragment.ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
            View view = layoutInflater
                    .inflate(R.layout.list_item_main, parent, false);
            return new ItemHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull PlaceholderFragment.ItemHolder holder, int position) {
            Item item = mItems.get(position);
            holder.bindItem(item);
        }

        @Override
        public int getItemCount() {
            return mItems.size();
        }
    }


    private void updateUI(int i) {
        DataSource ds = DataSource.INSTANCE;
        List<Item> items = new ArrayList<>(0);
        switch (i) {
            case 1: {
                items.addAll(ds.getLectures());
                break;
            }
            case 2: {
                items.addAll(ds.getPractices());
                break;
            }
            case 3: {
                items.addAll(ds.getTests());
                break;
            }
        }
        mAdapter = new ItemAdapter(items);
        recyclerView.setAdapter(mAdapter);
    }
}