package com.github.tamafjpj.libsat;

import java.util.UUID;

public class Item {
    private String mType;
    private String mName;
    private String mDescription;
    private String mStatus;
    private String mSource;
    private UUID mId;

    public Item(String mType, String mName, String mDescription) {
        this.mType = mType;
        this.mName = mName;
        this.mDescription = mDescription;
        this.mId = UUID.randomUUID();
    }

    public String getName() {
        return mName;
    }

    public void setName(String Name) {
        this.mName = Name;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String Description) {
        this.mDescription = Description;
    }

    public UUID getId() {
        return mId;
    }

    public String getType() {
        return mType;
    }

    public String getStatus() {
        return mStatus;
    }

    public void setStatus(String mStatus) {
        this.mStatus = mStatus;
    }

    public void setSource(String mSource) {
        this.mSource = mSource;
    }

    public String getSource() {
        return mSource;
    }
}
