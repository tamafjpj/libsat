package com.github.tamafjpj.libsat;

public class Test extends Item {
    private String status = "Не пройден";

    public Test(String name, String description, String source) {
        super("Test", name, description);
        super.setStatus(status);
        super.setSource(source);
    }
}