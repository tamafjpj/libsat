import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.sql.*;
import java.util.ArrayList;

public enum DBService {
    INSTANCE;

    private final String url = "jdbc:mysql://localhost:3306/libsat?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private final String user = "root";
    private final String password = "root";
    private Connection con;
    private Statement stmt;
    private GsonBuilder builder=new GsonBuilder();
    private Gson gson;
    DBService() {
        try {
            this.con = DriverManager.getConnection(url, user, password);
            this.stmt = con.createStatement();
        } catch (SQLException e) {
            System.err.println("Cant connect to database");
            e.printStackTrace();
        }
        builder.setPrettyPrinting();
        gson=builder.create();
    }

    public void close() {
        try {
            this.con.close();
            this.stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public String getLectures(String id) {
        ArrayList<Lecture> lectureList = new ArrayList<>(0);
        try {
            ResultSet rs = stmt.executeQuery("SELECT * FROM lectures WHERE group_id =" + "\""+id+"\"");
            while (rs.next()) {
                lectureList.add(new Lecture(rs.getString(2), rs.getString(3), rs.getString(4)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return gson.toJson(lectureList);
    }

    public String getPractices(String id) {
        ArrayList<Practice> practiceList = new ArrayList<>(0);
        try {
            ResultSet rs = stmt.executeQuery("SELECT * FROM practices WHERE group_id = "+"\""+id+"\"");
            while (rs.next()) {
                practiceList.add(new Practice(rs.getString(2), rs.getString(3), rs.getString(4)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return gson.toJson(practiceList);
    }

    public String getTests(String id) {
        return null;
    }
}
