
public class Item {
    private String mName;
    private String mDescription;
    public Item(String mName,String mDescription)
    {
        this.mName=mName;
        this.mDescription=mDescription;
    }
    public String getName() {
        return mName;
    }

    public void setName(String Name) {
        this.mName = Name;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String Description) {
        this.mDescription = Description;
    }
}